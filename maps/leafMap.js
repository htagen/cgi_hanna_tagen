
var map = L.map( 'map', {
  center: [20.0, 5.0],
  minZoom: 2,
  zoom: 3,
  zoomControl: false
});

//zoom button position
map.addControl(L.control.zoom({position: 'topright'}));

L.tileLayer( 'https://a.tile.openstreetmap.org/{z}/{x}/{y}.png', {
  attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
  subdomains: ['a', 'b', 'c']
}).addTo( map );

var myURL = jQuery( 'script[src$="leafMap.js"]' ).attr( 'src' ).replace( 'leafMap.js', '' );

//PINS

var pinYellow = L.icon({
  iconUrl: myURL + 'images/pinYellow.png',
  iconRetinaUrl: myURL + 'images/pinYellow.png',
  iconSize: [24, 24],
  iconAnchor: [9, 21],
  popupAnchor: [0, -14]
});

var pinGreen = L.icon({
    iconUrl: myURL + 'images/pinGreen.png',
    iconRetinaUrl: myURL + 'images/pinGreen.png',
    iconSize: [24, 24],
    iconAnchor: [9, 21],
    popupAnchor: [0, -14]
});

var pinRed = L.icon({
    iconUrl: myURL + 'images/pinRed.png',
    iconRetinaUrl: myURL + 'images/pinRed.png',
    iconSize: [24, 24],
    iconAnchor: [9, 21],
    popupAnchor: [0, -14]
});

// Saved markers from database -> map

var markersArray = new Array();
var markerNames = new Array();
var closestObjName;
var closestObjDis;


for (var i=0; i < markers.length; ++i) {
    var lat1 = markers[i].latJS;
    var lng1 = markers[i].lngJS;
    var name1 = markers[i].nameJS;
    var desc1 = markers[i].descJS;
    markersArray.push(L.marker([parseFloat(lat1), parseFloat(lng1)], {icon: pinGreen}).bindPopup('<p id="markerName">' + name1 + '</p><p id="markerDesc">' + desc1 + '</p>').addTo(map));
    markerNames.push(name1);
}

function closeObjects() {

    closestObjName = new Array();
    closestObjDis = new Array();

    for (var i=0; i < markersArray.length; i++) {
        var marker1 = markersArray[i];
        var smallestDis = null;
        var objName;
        for (var j=0; j < markersArray.length; j++) {
            if(i !== j) {
                var marker2 = markersArray[j];
                var tempDistance = map.distance(marker1.getLatLng(),marker2.getLatLng());
                if (tempDistance <= 500) {
                    marker1.setIcon(pinRed);
                    marker2.setIcon(pinRed);
                }
                if (smallestDis === null || tempDistance < smallestDis) {
                    smallestDis = tempDistance;
                    objName = markerNames[j];
                }
            }
        }
        closestObjName.push(objName);
        closestObjDis.push(smallestDis);
    }
}

//##############    ADD MARKERS #################

var addBtnPressed = false;
var markerTemp;
var nameTemp;

window.onload = function () {
    var lat, lng;
    var addBtn = document.getElementById("add");
    disableSaveBtn();
    closeObjects();

    map.on('click', function(e) {
        if(addBtnPressed) {
            var coords = document.getElementById('coord').value;
            nameTemp = document.getElementById('name').value;
            var desc = document.getElementById('desc').value;
            if (nameTemp !== "" && desc !== "") {
                lat = e.latlng.lat;
                lng = e.latlng.lng;
                document.getElementById('coord').value = parseFloat(lat).toFixed(7) + ', ' + parseFloat(lng).toFixed(7);
                if(coords !== ""){
                    map.removeLayer(markerTemp);
                }
                markerTemp = L.marker([lat, lng], {icon: pinYellow}).bindPopup('<p id="markerName">' + nameTemp + '</p><p id="markerDesc">' + desc + '</p>').addTo(map);
                enableSaveBtn();
                deactivateAddBtn();
            } else if (nameTemp === "" && desc === "") {
                alert("Sisesta nimi ja kirjeldus!");
            } else if (nameTemp === "") alert("Sisesta nimi!");
            else if (desc === "") alert("Sisesta kirjeldus!");
        }
        addRowHandlers();
    });

    document.getElementById("mySidenav").onclick =  function () {
        if (addBtn.active){
            stayActive();
        } else addBtnPressed = false;
        getClosestObjects();
        addRowHandlers();
    };

    document.getElementById("sideTable").onclick = addRowHandlers();
    document.getElementById("sideTable").onload = getClosestObjects();
    document.getElementById("sideTable").onclick = getClosestObjects();
};

//###################   BUTTON ACTIONS  ######################

function stayActive() {
    addBtnPressed = true;
    document.getElementById("add").active = true;
    document.getElementById("add").style.backgroundColor = "#4258b6";
}

function deactivateAddBtn() {
    addBtnPressed = false;
    document.getElementById("add").active = false;
    document.getElementById("add").style.backgroundColor = "#4b81e8";
}

function disableSaveBtn() {
    document.getElementById("save").disabled = true;
    document.getElementById("save").style.backgroundColor = "dimgray";
    document.getElementById("save").style.cursor = "default";
}

function enableSaveBtn(){
    document.getElementById("save").disabled = false;
    document.getElementById("save").style.backgroundColor = "rgb(132,219,48)";
}

function cancelBtnAction() {
    deactivateAddBtn();
    disableSaveBtn();
    document.getElementById("coord").value = "";
    document.getElementById("name").value = "";
    document.getElementById("desc").value = "";

    map.removeLayer(markerTemp);
}

function saveBtnAction() {
    sendData();
    deactivateAddBtn();
    disableSaveBtn();

    document.getElementById("coord").value = "";
    document.getElementById("name").value = "";
    document.getElementById("desc").value = "";

    markerTemp.setIcon(pinGreen);
    markersArray.push(markerTemp);
    markerNames.push(nameTemp);
    closeObjects();
    markerTemp = null;
    addRowHandlers();
}

function refresh() {
    getClosestObjects();
    addRowHandlers();
}

function sendData() {
    var data = {
        coordP: document.getElementById("coord").value,
        nameP: document.getElementById("name").value,
        descP: document.getElementById("desc").value
    };

    $.post("database/server.php", data);
}


// ##################   TABLE   #####################


var id;

function addRowHandlers() {
    var table = document.getElementById("table");
    var rows = table.getElementsByTagName("tr");
    for (i = 0; i < rows.length-1; i++) {
        var s = i+1;
        var currentRow = rows[s];
        var currentMarker = markersArray[i];
        var deleteBtn = currentRow.cells[6];
        id = currentRow.cells[0].innerHTML;

        var createClickHandler = function(marker) {
            return function() {
                marker.fire('click');
            };
        };

        var deleteHandler = function(marker, id, array, names, button, row) {
            return function() {
                map.removeLayer(marker);
                button.innerHTML = "";
                row.style.backgroundColor = "rgba(0,0,0,0.13)";
                row.cells[4].innerHTML = "";
                row.cells[5].innerHTML = "";

                var data = {
                    markerID: id
                };

                $.post("database/server.php", data);
            };
        };

        currentRow.onclick = createClickHandler(currentMarker);
        deleteBtn.onclick = deleteHandler(currentMarker, id, markersArray, markerNames, deleteBtn, currentRow);
    }
}

//###################   LOAD OBJECTS TO TABLE   ######################

$(document).ready(function () {
    $("#save").click(function () {
        $("#loadTable").load("https://www.mapify.ga/database/table.php");
    });
    $("#refresh").click(function () {
        $("#loadTable").load("https://www.mapify.ga/database/table.php");
    });
});

function getClosestObjects() {
    closeObjects();
    var table = document.getElementById("table");
    var rows = table.getElementsByTagName("tr");
    for (i = 0; i < rows.length-1; i++) {
        var currentRow = rows[i+1];
        currentRow.cells[4].innerHTML = closestObjName[i];
        currentRow.cells[5].innerHTML = closestObjDis[i];
    }
}
