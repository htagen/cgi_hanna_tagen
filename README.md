# CGI suvepraktika katseülesanne 2018 (TARTU)

https://www.mapify.ga/

Tööle kulunud aeg: ca 35h

Valisin kaardimootoriks Leaflet'i, kuna polnud varasemalt ise kaardimootoritega kokku puutunud. Leaflet oli üsna kerge, samuti läks väga kergelt kujundus (CSS, sidebar'id ja tabel),
samuti oli lihtne ka andmebaasiga ühendamine. 


Natuke oli segane, kas lõpptulemus peab olema reaalne veebirakendus või ei (ehk siis serveris), aga kuna ma olin serveri ülespanemisega, domeeni hankimise ja 
serveriga sidumisega varasemalt kokku puutunud, siis läks see väga kiiresti ja otsustasin esitada oma töö reaalselt veebis oleva rakendusena. 
Kasutasin DigitalOcean'i Ubuntu virtuaalserverit, millele installisin Apache2'e. Domeeni sain tasuta saidilt http://www.freenom.com. Tegin 
ka SSL sertifikaadi ning lisasin saidile cookie notice'i. 


Kõige rohkem aega võttis minu jaoks Javascripti loogika implementeerimine, kuna olen Javascriptiga kokku puutunud vaid mõned kuud ning kuna Javascript on väga liberaalne 
keel, siis tekitas see mõneti segadust (nt muutujatel ei ole defineeritud, mis liiki isendid nad on jms) ehk siis tuli erroreid kohtadelt, kust ma neid ei oodanud.


Töö tegin täielikult ise; kasutatud on Google abi ja varasemate projektide lahendusi.


Töösse jäi sisse üks (teadaolev) bug: peale tabelist rea kustutamist ja nupule ''värskenda'' vajutamist arvutab programm lähimaid kauguseid ja nimesid valesti, 
kuid leheküljele refresh'i tehes arvutab taas õigesti. Ning lisaks mõndadel üksikutel juhtudel ei värskenda tabel sisu, kui uus objekt salvestatakse, kuid jällegi aitab sellele 
refresh.


JUHEND:

* Täida tabelis väljad ''Nimi'' ja  ''Kirjeldus'';
* Vajuta nupule ''Märgi kaardile'';
* Vali kaardil sobiv asukoht ja kliki kaardil;
* Kui soovid objekti asukohta muuta, siis vajuta uuesti ''Märgi kaardil'' ja kliki uuesti kaardil;
* Salvestamata objekti kustutamiseks ja väljade tühjendamiseks vajuta ''Kustuta'';
* Salvestamiseks vajuta ''Salvesta'';
* Salvestatud objekti asukohta muuta ei saa;
* Tabelist (Salvestatud objektid) saad ridadele klikkides objekte valida (ilmub popup);
* Tabelist saad objekte kustutada (kaardilt ja andmebaasist) klikkides nupule ''Kustuta'';
* Kustutatud read muutuvad halliks, objekt kaob kaardilt;
* Tabeli värskendamiseks vajuta ''Värskenda''.

NB! Tabelis olev ID(#) näitab andmebaasis tulevat järjekorranumbrit/ID'd.

PS! Lehte avades saab kiirelt fade-effektiga mapify.ga logo eest ära, klikkides hiirega suvalisse kohta.
