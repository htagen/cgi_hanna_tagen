// JavaScript for label effects only
$(window).load(function(){
    $(".col-3 textarea input").val("");

    $(".input-effect textarea input").focusout(function(){
        if($(this).val() != ""){
            $(this).addClass("has-content");
        }else{
            $(this).removeClass("has-content");
        }
    })
});
