<?php
ini_set('display_errors',1);
error_reporting(E_ALL);

include_once 'helper.php';

$servername = "hostname";
$username = "username";
$password = "password";
$dbname = "name";

$connection = mysqli_connect(config($servername), config($username), config($password), config($dbname));
if ($connection->connect_error) {
    die("Connection failed: " . $connection->connect_error);
}

$errors = array();

$sql = 'select * from object';
$connection->set_charset('utf8');
$result = $connection->query($sql);

$output = array();

if ($result === false){
    echo 'Tekkis probleem!';
}
else if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        $array = explode(", ", $row['coordinates']);
        $lat = $array[0];
        $lng = $array[1];
        $name = $row['name'];
        $desc = $row['description'];

        $formData = array(
            'latJS' => $lat,
            'lngJS' => $lng,
            'nameJS' => $name,
            'descJS' => $desc
        );

        array_push($output, $formData);
    }
}

$connection->close();