<?php
ini_set('display_errors',1);
error_reporting(E_ALL);

include_once 'helper.php';


$servername = "hostname";
$username = "username";
$password = "password";
$dbname = "name";

$connection = mysqli_connect(config($servername), config($username), config($password), config($dbname));
if ($connection->connect_error) {
    die("Connection failed: " . $connection->connect_error);
}

$sql = 'select * from object';
$connection->set_charset('utf8');
$result = $connection->query($sql);

if ($result === false){
    echo 'Tekkis probleem!';
}
else if ($result->num_rows > 0) {
    echo '<table class="table" id="table"><tr id="firstRow"><th>#</th><th>Nimi</th><th>Kirjeldus</th><th>Koordinaadid</th><th>Lähim objekt</th><th>Kaugus</th><th>&times;</th>';
    while($row = $result->fetch_assoc()) {
        echo '<tr>
            <td class="markerID">' . $row['ID']. '</td>
            <td class="nameCol">' . $row['name']. ' </td>
            <td> ' . $row['description']. '</td>
            <td> ' . $row['coordinates']. ' </td>
            <td class="closestObj"></td>
            <td class="distance"></td>
            <td><button class="button cancel delete deleteButton">
                    <span class="btnText">kustuta</span>
                </button></td>';
    }
    echo '</table>';
}

$connection->close();
?>
<script>getClosestObjects()</script>
