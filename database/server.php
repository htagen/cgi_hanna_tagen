<?php
ini_set('display_errors',1);
error_reporting(E_ALL);

include_once 'helper.php';

$servername = "hostname";
$username = "username";
$password = "password";
$dbname = "name";

$connection = mysqli_connect(config($servername), config($username), config($password), config($dbname));
if ($connection->connect_error) {
    die("Connection failed: " . $connection->connect_error);
}

$errors = array();

//if user has set the button ''salvesta'', get the following data from database
if (isset($_POST['coordP'])) {

    $coord = $_POST['coordP'];
    $name = $_POST['nameP'];
    $desc = $_POST['descP'];

    if (empty($coord)) {
        array_push($errors, "Koordinaadid puuduvad!");
    }
    if (empty($name)) {
        array_push($errors, "Nimi puudub!");
    }
    if (empty($desc)) {
        array_push($errors, "Kirjeldus puudub!");
    }

    if (count($errors) == 0) {
        $query = "INSERT INTO object (coordinates, name, description) VALUES ('$coord', '$name', '$desc')";
        try {
            mysqli_query($connection, $query);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
}

//get markers id from javascript and delete the marker from database

if (isset($_POST['markerID'])) {

    $ID = $_POST['markerID'];

    if (empty($ID)) {
        array_push($errors, "ID puudub!");
    }

    if (count($errors) == 0) {
        $query = "delete from object where ID='$ID'";
        try {
            mysqli_query($connection, $query);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
}

$connection->close();