<!DOCTYPE html>
<html lang="en-US" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php include 'head.php'?>
</head>
<body id="mainBody">
<?php include ('database/server.php') ?>
<div id="fullScreen" onclick="clearFade()">
    <div id="landingContainer">
        <div><h1 id="logoName">mapify.ga</h1></div>
        <span><img alt="mapify.ga logo" id="logoImg" title="mapify" src="images/pin.svg"></span>
    </div>
</div>
<?php include 'database/getMarkers.php'?>
<div id="map"></div>
<script>var markers = <?php echo json_encode($output, JSON_PRETTY_PRINT) ?></script>
<script src='maps/leafMap.js'></script>
<span id="sideNavOpenButton" onclick="openNav()"><img alt="Menüü" id="plusIcon" src="images/plus.svg"></span>
<div id="mySidenav" class="sidenav">
    <div class="menuElements">
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
        <a id="trigger" href="javascript:void(0)" onclick="openTable()" title="Salvestatud objektid">• Salvestatud objektid</a><br>
    </div>
    <div id="mainForm">
        <div class="col-3">
            <input title="Koordinaadid" id="coord" disabled class="effect-2" type="text" name="coordinates" placeholder="Koordinaadid">
            <span class="focus-border"></span>
        </div>
        <div class="col-3">
            <input title="Nimi" id="name" class="effect-2" required type="text" name="name" placeholder="Nimi" oninvalid="this.setCustomValidity('Sisesta nimi!')" oninput="setCustomValidity('')">
            <span class="focus-border"></span>
        </div>
        <div class="col-3">
            <textarea title="Kirjeldus" id="desc" required name="description" placeholder="Kirjeldus" oninvalid="this.setCustomValidity('Sisesta kirjeldus!')" oninput="setCustomValidity('')" rows="1" cols="5" wrap="soft"></textarea>
            <span class="focus-border ta"></span>
        </div>
        <div id="buttonContainer">
            <button class="button add" id="add" onclick="stayActive()" type="button">
                <span>märgi kaardile</span>
            </button><br>
            <button class="button cancel" onclick="cancelBtnAction()" type="reset">
                <span>katkesta</span>
            </button><br>
            <button type="submit" class="button save" id="save" onclick="saveBtnAction()" name="save" >
                <span>salvesta</span>
            </button>
        </div>
    </div>
</div>
<div id="sideTable" class="sidenav">
    <div class="menuElements">
        <a href="javascript:void(0)" class="closebtn" onclick="closeTable()">&times;</a>
<!--        <span id="savedObj">• Salvestatud objektid</span>-->
        <button class="button add" onclick="refresh()" id="refresh" type="button">
            <span>värskenda</span>
        </button><br>
        <br><br>
    </div>
    <div id="loadTable">
        <?php include 'database/table.php' ?>
    </div>
</div>
<div id="mainLogoContainer">
    <a id="logoNameMain" href="https://mapify.ga">mapify.ga<img alt="mapify.ga logo" id="logoImgMain" title="mapify" src="images/pin.svg"></a>
</div>
</body>
</html>
