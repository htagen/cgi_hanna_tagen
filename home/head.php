    <?php error_reporting(E_ALL); ini_set('display_errors', 'on'); ?>
    <title>mapify.ga</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="author" content="Hanna Tagen">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<!--    <meta property="og:image" content="http://askut.today/images/preview_image_logo_plane3.png" />-->

    <!-- css failid -->
    <link href="styles/stylesheet.css" rel="stylesheet" type="text/css">
    <link href="styles/landing.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css" />
    <link href="styles/map.css" rel="stylesheet" type="text/css">
    <link href="styles/table.css" rel="stylesheet" type="text/css">

    <!-- skriptid -->
    <script src='https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js' rel="script"></script>
    <script src="https://code.jquery.com/jquery-1.11.1.min.js" rel="script"></script>
    <script src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js" rel="script"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js" rel="script"></script>
    <script src="scripts/cookies.js" rel="script"></script>
    <script src="scripts/landingFade.js" rel="script"></script>
    <script src="scripts/sidebar.js" rel="script"></script>
    <script src="scripts/input.js" rel="script"></script>
    <script src="scripts/button.js" rel="script"></script>

    <!--    varia-->
    <link rel="icon" href="images/pin2.png">
    <link rel="shortcut icon" href="images/pin2.png" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Lato" />
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />